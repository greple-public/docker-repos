IMAGE_TAG=registry.gitlab.com/greple-public/docker-repos/pyspark-notebook:with-ts
docker build --rm --force-rm \
    -t $IMAGE_TAG .  && \
    docker images $IMAGE_TAG && \
    docker image push $IMAGE_TAG 